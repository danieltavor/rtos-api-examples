
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <mongoc/mongoc.h>

void *print_message_function( void *ptr );

typedef struct{
    char* str;
    int num;
} InputStc;

int main()
{
     pthread_t thread1, thread2;

     InputStc stc1;
     InputStc stc2;
     stc1.str = "Thread 1";
     stc2.str = "Thread 2";
     stc1.num = 9;
     stc2.num = 12;

     int  iret1, iret2;

    /* Create independent threads each of which will execute function */

     iret1 = pthread_create( &thread1, NULL, print_message_function, (void*) &stc1);
     iret2 = pthread_create( &thread2, NULL, print_message_function, (void*) &stc2);

     /* Wait till threads are complete before main continues. Unless we  */
     /* wait we run the risk of executing an exit which will terminate   */
     /* the process and all threads before the threads have completed.   */

     pthread_join( thread1, NULL);
     pthread_join( thread2, NULL); 

     printf("Thread 1 returns: %d\n",iret1);
     printf("Thread 2 returns: %d\n",iret2);

     exit(0);
}

void *print_message_function( void *ptr )
{
    InputStc* stc;
    stc = (InputStc*) ptr;
    printf("%s [%d]\n", stc->str,stc->num);
    pthread_exit(NULL);
}
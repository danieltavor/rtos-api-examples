
#include <stdio.h>
#include <math.h>

void main(){

    float w=0.0;
    float h=0.0;
    float bmi = 0.0;

    printf("Enter your Height in [m]\r\n");
    scanf("%f",&h);
    printf("Enter your Weight in [kg]\r\n");
    scanf("%f",&w);

    bmi = w/pow(h,2);

    printf("Your BMI is: %g\n",bmi);

}

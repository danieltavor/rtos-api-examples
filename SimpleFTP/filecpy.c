#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
	int src, dst;
	int n;
	char buf[100];
	if (argc != 3) {
		printf ("usage : myfdcopy src dst\n");
		exit(1);
	}
	src = open ( argv[1], O_RDWR); 
	if (src == -1) { 
		printf ("can't open src file\n");
		exit(1);
	}
	dst = open (argv[2], O_CREAT | O_TRUNC | O_RDWR, S_IRWXU);
	if (dst == -1) { 
		printf ("can't open src file\n");
		exit(1);
	}
	while ((n=read(src, buf,100)) != 0) {
		write(dst, buf, n);
	}
	close(src);
	close(dst);



}

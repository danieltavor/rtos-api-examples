#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

void sigint_handler(int sig)
{
    write(0, "Ahhh! SIGINT!\n", 14);
}

void sigfpe_handler(int sig)
{
    write(0, "Ahhh! SIGFPE!\n", 14);
}

void sigalrm_handler(int sig)
{
    write(0, "Ahhh! SIGALRAM!\n", 18);
}


void sigalraise_handler(int sig)
{
    write(0, "Raise!\n", 14);
}


int main(void)
{

    char s[200];
    struct sigaction sa1,sa2,sa3,sa4;

    sa1.sa_handler = sigint_handler;
    sa1.sa_flags = 0; // or SA_RESTART
    sigemptyset(&sa1.sa_mask);

    sa2.sa_handler = sigfpe_handler;
    sa2.sa_flags = 0; // or SA_RESTART
    sigemptyset(&sa2.sa_mask);


    sa3.sa_handler = sigalrm_handler;
    sa3.sa_flags = 0; // or SA_RESTART
    sigemptyset(&sa3.sa_mask);

    sa4.sa_handler = sigint_handler;
    sa4.sa_flags = 0; // or SA_RESTART
    sigemptyset(&sa4.sa_mask);

    if (sigaction(SIGINT, &sa1, NULL) == -1) {
        perror("sigaction");
        exit(1);
    }

    if (sigaction(SIGFPE, &sa2, NULL) == -1) {
        perror("sigaction");
        exit(2);
    }

    if (sigaction(SIGALRM, &sa3, NULL) == -1) {
        perror("sigaction");
        exit(2);
    }

    if (sigaction(SIGINT, &sa4, NULL) == -1) {
        perror("sigaction");
        exit(2);
    }

    printf("Enter a string:\n");

    if (fgets(s, sizeof s, stdin) == NULL)
        perror("fgets");
    else 
        printf("You entered: %s\n", s);


    alarm(2);
    sleep(4);

    raise(SIGINT);

    int i=0;
    int k=0;
    int m = i/k;

    return 0;
}

/*
** client.c -- a stream socket client demo
*/

#include <stdio.h>
#include <poll.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#define PORT "9034" // the port client will be connecting to 

#define MAXDATASIZE 100 // max number of bytes we can get at once 

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char *argv[])
{
	int sockfd, numbytes;  
	char buf[MAXDATASIZE];
	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];

	if (argc != 2) {
	    fprintf(stderr,"usage: client hostname\n");
	    exit(1);
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			perror("client: connect");
			close(sockfd);
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
			s, sizeof s);
	printf("client: connecting to %s\n", s);
	
	freeaddrinfo(servinfo); // all done with this structure

	// Create a struct for 2 polls to lisining to: stdin and connection with server
	struct pollfd pfds[2];

	// STDIN
	pfds[0].fd  = STDIN_FILENO;
	pfds[0].events = POLLIN;

	// Connection with server
	pfds[1].fd = sockfd;
	pfds[1].events = POLLIN;

	// Poll for ever!
	while(1) {

		poll (pfds, 2, -1); // Poll two sources without timeout

		// Stdin event rise
		if (pfds[0].revents & POLLIN) { 
			numbytes = read (STDIN_FILENO, buf, MAXDATASIZE-1); // Read from stdin
			send(sockfd, buf, numbytes,0); // Sent to server via socket

		// Event in server socket
		} else if(pfds[1].revents & POLLIN){
			numbytes = recv (sockfd, buf, MAXDATASIZE-1,0); // Retrive data from server
			buf[numbytes]='\0';	// Terminate message using NULL terminator for strings
			printf ("server:%s\n",buf); // Print to stdout
		}
	}

	close(sockfd);

	return 0;
}

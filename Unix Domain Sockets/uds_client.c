/*
** client.c -- a stream socket client demo
*/

#include <stdio.h>
#include <poll.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#define MAXDATASIZE 100 // max number of bytes we can get at once 
#define SOCK_PATH "echo_socket"

int main(int argc, char *argv[])
{
	int sockfd, numbytes;  
	char buf[MAXDATASIZE];
	struct sockaddr_un remote;
	int len;

	sockfd=socket(AF_UNIX, SOCK_STREAM, 0);
	printf("Trying to connect...\n");

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, SOCK_PATH);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(sockfd, (struct sockaddr *)&remote, len) == -1) {
        perror("connect");
        exit(1);
    }

    printf("Connected.\n");

	struct pollfd pfds[2];
	pfds[0].fd  = 0; // Stdin
	pfds[0].events = POLLIN;
	pfds[1].fd = sockfd;
	pfds[1].events = POLLIN;
	for (;;) {
		poll ( pfds, 2, -1);
		if (pfds[0].revents & POLLIN) { 
			numbytes = read (0, buf, MAXDATASIZE-1);	
			send ( sockfd, buf, numbytes,0);
		} else {
			numbytes = recv (sockfd, buf, MAXDATASIZE-1,0);
			buf[numbytes]=0;
			printf ("server:%s\n",buf);
		}
	}
	close(sockfd);

	return 0;
}

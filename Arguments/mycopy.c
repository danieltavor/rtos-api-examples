#include <stdio.h>
#include <stdlib.h>

int main(int argc , char * argv[])
{
	FILE * src;
	FILE * dst;
	char c;
	if (argc !=3) {
		printf ("Usage : copy src dst\n");
		exit(1);
	}
	src = fopen(argv[1], "r");
	if (src == NULL) {
		printf ("can't open src file\n");
		exit(1);
	}
	dst = fopen(argv[2], "w+");
	if (dst == NULL) {
		printf("Can't open dst file\n");
		exit(1);
	}
	while (!(feof(src))) {
		fread(&c, 1, 1, src);
		fwrite(&c, 1,1, dst);
	}
	fclose (src);
	fclose (dst);
}


#include <stdio.h>
#include <stdlib.h>


int main(int argc, char*argv[]){

   char c=0;

    if (argc != 3){
        printf("Function should have 2 arguments!\n");
        exit(1);
    }

    FILE *src = fopen(argv[1],"r");
    FILE *dst = fopen(argv[2],"w+");

    if (src == NULL){
        printf("Couldnt open source file!\n");
        exit(2);
    }

    if (dst == NULL){
        printf("Couldnt create destination file!\n");
        exit(3);
    }

    c = fgetc(src);
    while(c != EOF){
        fputc(c,dst);
        c = fgetc(src);
    }

    fclose(src);
    fclose(dst);

    return 0;
}
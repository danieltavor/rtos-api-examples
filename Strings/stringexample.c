
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int mystrlen(char *s){

    int i=0;
    int len=0;

    while(s[i] != '\0'){
        len++;
        i++;
    }

    return len;

}

char* strutol(char *s){
    int len = strlen(s);
    char* o_str = (char*) malloc(len*sizeof(char));

    for (int i=0;i<len;i++){
        if (s[i] >= 'A' && s[i]<='Z')
            o_str[i] = s[i] + ('a'-'A');
        else
            o_str[i] = s[i];
    }

    return o_str;
}

char* strrev(char *s){

    int len = strlen(s);
    char* srv = (char*) malloc(len*sizeof(char));

    for (int i=0; i<len;i++)
        srv[len-i-1]=s[i];

    return srv;

}

void chinstr(char *s,char c){
    int len = strlen(s);

    printf("%c is in the string at index: ",c);
    for (int i=0;i<len;i++){
        if (s[i]==c)
            printf("[%d] ",i);
    }
    printf("\n");
}

void main(){

    char* str = "Danielapa";
    char *v;
    printf("String Length is: %d\r\n",mystrlen(str));
    chinstr(str,'a');
    v = strrev(str);
    printf("%s\n",v);
    v = strutol(str);
    printf("%s\n",v);
    free(v);
    

}
